'use strict'


// ## Test one
step('test one', { continueOnFailure: true }, async function (done) {
  done();
})

step('test two', { continueOnFailure: true }, async function (done) {
  done();
})

step('test three', { continueOnFailure: true }, async function (done) {
  done();
})

step('test four', { continueOnFailure: true }, async function (done) {
  done();
})

step('test five', { continueOnFailure: true }, async function (done) {
  done();
})

step('test six', { continueOnFailure: true }, async function (done) {
  done();
})

step('test seven', { continueOnFailure: true }, async function (done) {
  done();
})

step('test eight', { continueOnFailure: true }, async function (done) {
  done();
})

step('test nine', { continueOnFailure: true }, async function (done) {
  done({ error: true });
})

step('test ten', { continueOnFailure: true }, async function (done) {
  done({ error: true });
})